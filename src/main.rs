use rand::Rng;
use std::io;

// struct to hold game settings.
struct Settings {
    width: usize,
    block_max_height: usize,
    turns: usize,
}

impl Settings {
    fn new(width: usize, block_max_height: usize, turns: usize) -> Self {
        Self{
            width: width,
            block_max_height: block_max_height,
            turns: turns,
        }
    }
}

// struct to be a stack of blocks
struct Stack {
    id: usize,
    blocks: String,
    height: usize,
}

impl Stack {
    fn new(id: usize) -> Self {
        Self{
            id: id,
            blocks: String::new(),
            height: 0,
        }
    }

    fn add_block(&mut self, block: &usize) {
        self.height = self.height + block;

        self.blocks = format!("{}{} ", self.blocks, block)
        
    }
}

// create a new block with a height between 1 and Settings.block_max_height
// returns a usize to represent the block's height
fn create_block(max: &usize) -> usize {
    let next_block: usize = rand::thread_rng().gen_range(1..=4);
    println!("Next block is {} tall.", next_block);
    return next_block;
}

// place a block onto a Stack
fn place_block(stacks: &mut Vec<Stack>, block: &usize, width: &usize) -> usize {
    println!("Where should it go? (1-4)");

    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");

    let input_num: usize = input.trim().parse().expect("Please type a number!");

    println!("You input: {input_num}");

    stacks[input_num - 1].add_block(block);

    print_stacks(&stacks);

    return input_num
}

// prints stack heights
// no return

fn print_stacks(stacks: &Vec<Stack>) {
   for i in stacks {
        println!("stack {}: {}", i.id, i.height);
        // println!("blocks {}", i.blocks);
   }
}

// counts how many Stacks are the same height as the block you last placed
// returns the score for this round only
fn count_score(stacks: &Vec<Stack>, stack_id: usize) -> usize {
    let mut score = 0;

    for i in stacks {
        if i.height == stacks[stack_id - 1].height {
            score += 1
        }
    }
    println!("this turn: {} points", score);
    return score;
 }

fn main() {
    let settings: Settings = Settings::new(4, 4, 10);
    let mut turns: usize = settings.turns;
    let mut score: usize = 0;

    let mut stacks: Vec<Stack> = vec![
        Stack::new(1),
        Stack::new(2),
        Stack::new(3),
        Stack::new(4),
    ];

    println!("");
    println!("Add blocks to the stacks.");
    println!("If that makes stacks the same height, you get points.");
    println!("The more stacks at that same height, the more points you get.");
    println!("");

    print_stacks(&stacks);

    while turns > 0 {
        let next_block = create_block(&settings.block_max_height);
	let stack_id = place_block(&mut stacks, &next_block, &settings.width);

        score += count_score(&stacks, stack_id);
	println!("total: {} points", score);
    	println!("");

        turns -= 1;
    }
}

//
// TODO
//
// Add Settings.block_max_height to create_block()
// ^ 'rand::thread_rng().gen_range(1..=max);' needs max to be an integer
//
// Settings.block_min_height?
//
// Add Settings.width to 'let mut stacks'
// ^ for i in (1..=settings.width)
//
// Add error handling and more input options to place_block()
//
// Add a way to change settings
//
// Add a better tui
